import { Component, OnInit } from '@angular/core';
import { SomeDataService } from '../../services/some-data.service';
import { SomeData } from '../../model/some-data';

@Component({
    selector: 'app-show-data',
    templateUrl: './show-data.component.html',
    styleUrls: ['./show-data.component.css']
})
export class ShowDataComponent implements OnInit {

    someData: SomeData;

    constructor(private readonly someDataService: SomeDataService) {
    }

    async ngOnInit() {
        this.someData = await this.someDataService.getSomeData();
    }

    async updateData() {
        await this.someDataService.updateSomeData(this.someData);
    }
}
