import { Injectable } from '@angular/core';
import { SomeData } from '../model/some-data';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class SomeDataService {

    private readonly url = 'some-url';

    constructor(private readonly httpClient: HttpClient) {
    }

    getSomeData(): Promise<SomeData> {
        return this.httpClient.get<SomeData>(this.url).toPromise();
    }

    updateSomeData(someData: SomeData): Promise<SomeData> {
        return this.httpClient.put<SomeData>(this.url, someData).toPromise();
    }
}
