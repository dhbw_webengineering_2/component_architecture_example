import { Component, EventEmitter, Input, Output } from '@angular/core';
import { InputData } from '../../model/input-data';
import { FormData } from '../../model/form-data';

@Component({
    selector: 'app-second-layer',
    templateUrl: './second-layer.component.html',
    styleUrls: ['./second-layer.component.css']
})
export class SecondLayerComponent {

    @Input()
    inputData: InputData;
    @Output()
    formDataChange = new EventEmitter<FormData>();

    onFormDataChanged(formData: FormData) {
        this.formDataChange.emit(formData);
    }
}
