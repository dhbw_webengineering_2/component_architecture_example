import { Component, EventEmitter, Input, Output } from '@angular/core';
import { InputData } from '../../model/input-data';
import { FormData } from '../../model/form-data';

@Component({
    selector: 'app-third-layer',
    templateUrl: './third-layer.component.html',
    styleUrls: ['./third-layer.component.css']
})
export class ThirdLayerComponent {

    @Input()
    inputData: InputData;
    formData: FormData = { someFormStringInput: null, someFormNumberInput: null};
    @Output()
    formDataChange = new EventEmitter<FormData>();

    onSubmit() {
        this.formDataChange.emit(this.formData);
    }
}
