import { Component } from '@angular/core';
import { InputData } from '../../model/input-data';
import { AccumulatedData } from '../../model/accumulated-data';
import { FormData } from '../../model/form-data';

@Component({
    selector: 'app-first-layer',
    templateUrl: './first-layer.component.html',
    styleUrls: ['./first-layer.component.css']
})
export class FirstLayerComponent {
    inputData: InputData = {
        someTitle: 'SomeTitle',
        someDescription: 'SomeDescription',
        text: 'lalala'
    };

    accumulatedData: AccumulatedData = { someMoreData: 'lalala' };

    onFormDataChanged(formData: FormData) {
        this.accumulatedData.formData = formData;
    }
}
