import { FormData } from './form-data';

export interface AccumulatedData {
    formData?: FormData;
    someMoreData: string;
}
