export interface InputData {
    someTitle: string;
    someDescription: string;
    text: string;
}
