import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ShowDataComponent } from './components/show-data/show-data.component';
import { SomeDataService } from './services/some-data.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FirstLayerComponent } from './component_hierarchy_example/components/first-layer/first-layer.component';
import { SecondLayerComponent } from './component_hierarchy_example/components/second-layer/second-layer.component';
import { ThirdLayerComponent } from './component_hierarchy_example/components/third-layer/third-layer.component';

@NgModule({
    declarations: [
        AppComponent,
        ShowDataComponent,
        FirstLayerComponent,
        SecondLayerComponent,
        ThirdLayerComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
    ],
    providers: [SomeDataService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
